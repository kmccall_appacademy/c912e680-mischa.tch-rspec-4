class Friend
  def greeting(someone = nil)
    if someone == nil
      return "Hello!"
    else
      return "Hello, #{someone}!"
    end
  end
end
