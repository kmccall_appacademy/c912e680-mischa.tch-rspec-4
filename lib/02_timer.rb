class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def padded (num)
    return num < 10? "0#{num}": "#{num}"
  end

  def hours
    (seconds / 3600).to_i
  end

  def minutes
  ((seconds % 3600) / 60).to_i
  end

  def sec
    (seconds % 60).to_i
  end

  def time_string
    "#{padded(hours)}:#{padded(minutes)}:#{padded(sec)}"
  end
end
