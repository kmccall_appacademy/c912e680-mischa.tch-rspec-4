class Dictionary

  attr_reader :entries

  def initialize
    @entries = Hash.new(0)
  end

  def add(entry)
    if entry.class == Hash
      @entries[entry.keys.last] = entry.values.last
    else
      @entries[entry] = nil
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(keyword)
    @entries.keys.include?(keyword)
  end

  def find(word)
    result = {}
    keywords.each do |key|
      if key.include?(word)
        result[key] = @entries[key]
      end
    end
    result
  end

  def printable
    res = []
    keywords.each {|key| res << "[#{key}] \"#{@entries[key]}\""}
    res.join("\n")
  end
end

# if __FILE__ == $PROGRAM_NAME
#   d = Dictionary.new
#   d.entries
# end
