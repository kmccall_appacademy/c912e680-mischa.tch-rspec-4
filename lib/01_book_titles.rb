class Book
  attr_reader :title


  def title=(title)
    skip_words = 'a and an or the over in of'.split(' ')
    words = title.downcase.split(' ')
    arr = []
    words.each_with_index do |word, idx|
      if idx != 0 && skip_words.index(word) != nil
        arr << word
      else
        arr << word.capitalize
      end
    end
    @title = arr.join(' ')
  end
end
